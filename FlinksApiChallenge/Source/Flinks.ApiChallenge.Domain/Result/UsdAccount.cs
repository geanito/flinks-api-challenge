﻿using Newtonsoft.Json;

namespace Flinks.ApiChallenge.Domain.Result
{
    public class UsdAccount
    {
        [JsonProperty("Balance")]
        public string Balance { get; set; }
    }
}
