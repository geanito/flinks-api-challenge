﻿using Newtonsoft.Json;

namespace Flinks.ApiChallenge.Domain.Result
{
    public class OperationAccount
    {
        [JsonProperty("AccountNumber")]
        public string AccountNumber { get; set; }
    }
}
