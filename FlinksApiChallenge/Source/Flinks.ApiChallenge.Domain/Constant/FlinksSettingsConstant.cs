﻿namespace Flinks.ApiChallenge.Domain.Constant
{
    public class FlinksSettingsConstant
    {
        public const string CustomerId = "Flinks.CustomerId";
        public const string UserName = "Flinks.UserName";
        public const string Password = "Flinks.Password";
        public const string Institution = "Flinks.Institution";


        public const string CustomerIdParameter = "{CustomerId}";
    }
}
