﻿namespace Flinks.ApiChallenge.Domain.Constant
{
    public static class AccountCategoryConstant
    {
        public const string Operations = "Operations";
        public const string Credits = "Credits";
        public const string Products = "Products";
    }
}
