﻿namespace Flinks.ApiChallenge.Domain.Constant
{
    public static class AccountTypeConstant
    {
        public const string Chequing = "Chequing";
        public const string CreditCard = "CreditCard";
        public const string RRSP = "RRSP";
        public const string LineOfCredit = "LineOfCredit";
        public const string LoanPersonal = "LoanPersonal";
    }
}
