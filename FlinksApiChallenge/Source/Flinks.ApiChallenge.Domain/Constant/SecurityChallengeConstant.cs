﻿namespace Flinks.ApiChallenge.Domain.Constant
{
    public static class SecurityChallengeConstant
    {
        //Questions
        public const string WhatCityWereYouBornIn = "What city were you born in?";
        public const string WhatIsTheBestCountryOnEarth = "What is the best country on earth?";
        public const string WhatIsTheShapePeopleLikeTheMost = "What shape do people like most?";

        //Answers
        public const string WhatCityWereYouBornInAnswer = "Montreal";
        public const string WhatIsTheBestCountryOnEarthAnswer = "Canada";
        public const string WhatIsTheShapePeopleLikeTheMostAnswer = "Triangle";
    }
}
