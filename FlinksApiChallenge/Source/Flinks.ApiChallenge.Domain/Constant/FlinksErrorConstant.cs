﻿namespace Flinks.ApiChallenge.Domain.Constant
{
    public static class FlinksErrorConstant
    {
        //ErrorMessages
        public const string ValidationProblems = "There were some validation problems.";


        //ErrorCodes
        public const string INVALID_LOGIN = "INVALID_LOGIN";
        public const string INVALID_USERNAME = "INVALID_USERNAME";
        public const string INVALID_PASSWORD = "INVALID_PASSWORD";
        public const string INVALID_SECURITY_RESPONSE = "INVALID_SECURITY_RESPONSE ";
        public const string QUESTION_NOT_FOUND = "QUESTION_NOT_FOUND";
        public const string UNKNOWN_CHALLENGE_KEY = "UNKNOWN_CHALLENGE_KEY";
        public const string SECURITYRESPONSES_INCOMPLETE = "SECURITYRESPONSES_INCOMPLETE";
        public const string AGGREGATION_ERROR = "AGGREGATION_ERROR";
        public const string DISABLED_INSTITUTION = "DISABLED_INSTITUTION";
        public const string RETRY_LATER = "RETRY_LATER";
        public const string INVALID_REQUEST = "INVALID_REQUEST";
        public const string OPERATION_DISPATCHED = "OPERATION_DISPATCHED";
        public const string OPERATION_PENDING = "OPERATION_PENDING";
        public const string CONCURRENT_SESSION = "CONCURRENT_SESSION";
        public const string UNAUTHORIZED = "UNAUTHORIZED";
        public const string ALREADY_AUTHORIZED = "ALREADY_AUTHORIZED";
        public const string SESSION_NONEXISTENT = "SESSION_NONEXISTENT";
        public const string DISABLED_LOGIN = "DISABLED_LOGIN";
        public const string NEW_ACCOUNT = "NEW_ACCOUNT";
        public const string TOO_MANY_REQUESTS = "TOO_MANY_REQUESTS";
        public const string CUSTOMER_NOT_FOUND = "CUSTOMER_NOT_FOUND";
        public const string NO_BROWSER_AVAILABLE = "NO_BROWSER_AVAILABLE";
        public const string SESSION_EXPIRED = "SESSION_EXPIRED";

    }
}
