﻿namespace Flinks.ApiChallenge.Domain.Constant
{
    public static class CurrencyConstant
    {
        public const string CAD = "CAD";
        public const string USD = "USD";
    }
}
