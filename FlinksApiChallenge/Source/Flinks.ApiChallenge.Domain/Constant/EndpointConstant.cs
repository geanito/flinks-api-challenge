﻿namespace Flinks.ApiChallenge.Domain.Constant
{
    public static class EndpointConstant
    {
        public const string BaseUrl = "https://sandbox.flinks.io/v3/{CustomerId}/BankingServices";
        public const string Authorize = "/Authorize";
        public const string GetAccountsSummary = "/GetAccountsSummary";
        public const string GetAccountsDetail = "/GetAccountsDetail ";
    }
}
