﻿namespace Flinks.ApiChallenge.Domain.Constant
{
    public static class ClientParameterConstant
    {
        //Header Parameters
        public const string ContentType = "Content-Type";
        public const string ApplicationJson = "application/json";
        public const string CharsetUTF8 = "charset=UTF-8";

        //BodyParamters
        public const string DaysOfTransactions = "DaysOfTransactions";
    }
}
