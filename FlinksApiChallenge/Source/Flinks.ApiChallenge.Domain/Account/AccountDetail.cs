﻿using System.Collections.Generic;
using Flinks.ApiChallenge.Domain.Shared;
using Newtonsoft.Json;

namespace Flinks.ApiChallenge.Domain.Account
{
    public class AccountDetail : FlinksRoot
    {
        [JsonProperty("Accounts")]
        public List<Account> Accounts { get; set; }

        [JsonProperty("Login")]
        public Login Login { get; set; }

    }
}
