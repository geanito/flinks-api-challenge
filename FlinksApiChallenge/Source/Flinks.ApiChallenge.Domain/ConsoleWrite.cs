﻿using System;

namespace Flinks.ApiChallenge.Domain
{
    public static class ConsoleWrite
    {
        public static void Line(string textInput)
        {
            Console.WriteLine($"{DateTime.Now} - {textInput}");
        }
        public static void LineWithNoDateTime(string textInput)
        {
            Console.WriteLine(textInput);
        }
        public static void BlankLine()
        {
            Console.WriteLine(string.Empty);
        }
    }
}
