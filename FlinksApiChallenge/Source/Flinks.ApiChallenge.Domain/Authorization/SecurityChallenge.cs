﻿using Newtonsoft.Json;

namespace Flinks.ApiChallenge.Domain.Authorization
{
    public class SecurityChallenge
    {
        [JsonProperty("Type")]
        public string Type { get; set; }
        [JsonProperty("Prompt")]
        public string Prompt { get; set; }
    }
}
