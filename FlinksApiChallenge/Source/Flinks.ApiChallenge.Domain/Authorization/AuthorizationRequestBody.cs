﻿using System.Collections.Generic;
using Flinks.ApiChallenge.Domain.Shared;
using Newtonsoft.Json;

namespace Flinks.ApiChallenge.Domain.Authorization
{
    public class AuthorizationRequestBody : FlinksRoot
    {
        public AuthorizationRequestBody()
        {
            Save = "true";
            RequestId = null;
        }

        [JsonProperty("UserName")]
        public string UserName { get; set; }
        [JsonProperty("Password")]
        public string Password { get; set; }
        [JsonProperty("LoginId")]
        public string LoginId { get; set; }
        [JsonProperty("SecurityResponses")]
        public Dictionary<string, List<string>> SecurityResponses { get; set; }
        [JsonProperty("Save")]
        public string Save { get; set; }
    }
}
