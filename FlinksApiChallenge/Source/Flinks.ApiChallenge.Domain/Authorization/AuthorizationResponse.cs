﻿using System.Collections.Generic;
using Flinks.ApiChallenge.Domain.Shared;
using Newtonsoft.Json;

namespace Flinks.ApiChallenge.Domain.Authorization
{
    public class AuthorizationResponse : FlinksRoot
    {
        public AuthorizationResponse()
        {
            RequestId = null;
        }

        [JsonProperty("Links")]
        public List<Link> Links { get; set; }
        [JsonProperty("SecurityChallenges")]
        public List<SecurityChallenge> SecurityChallenges { get; set; }
        [JsonProperty("Login")]
        public Login Login { get; set; }
    }
}
