﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Flinks.ApiChallenge.Domain.Shared
{
    public class FlinksRoot
    {
        [JsonProperty("HttpStatusCode", NullValueHandling = NullValueHandling.Ignore)]
        public int HttpStatusCode { get; set; }
        [JsonProperty("Institution")]
        public string Institution { get; set; }
        [JsonProperty("RequestId")]
        public string RequestId { get; set; }
        [JsonProperty("ValidationDetails", NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, List<string>> ValidationDetails { get; set; }
    }
}
