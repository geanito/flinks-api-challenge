﻿using System;
using System.Collections.Generic;
using System.Linq;
using Flinks.ApiChallenge.Client;
using Flinks.ApiChallenge.Domain;
using Flinks.ApiChallenge.Domain.Account;
using Flinks.ApiChallenge.Domain.Constant;
using Flinks.ApiChallenge.Domain.Result;
using Flinks.ApiChallenge.Domain.Shared;
using Newtonsoft.Json;
using Console = System.Console;

namespace Flinks.ApiChallenge.ConsoleApp
{
    class Program
    {
        static void Main()
        {
            ConsoleWrite.Line("Flinks API Challenge");
            var keepAsking = true;

            while (keepAsking)
            {
                ConsoleWrite.Line("Please wait, trying to retrieve the results from the API...");

                var accountDetail = LoadData();

                if (accountDetail == null)
                {
                    ConsoleWrite.Line("Failed to retrieve the API data. Do you want to try again? (Y/N)");
                    var answer = Console.ReadLine() ?? string.Empty;

                    switch (answer)
                    {
                        case "Y":
                        {
                            ConsoleWrite.Line("OK! Let's try again...");
                            break;
                        }
                        case "N":
                        {
                            ConsoleWrite.Line("OK. Bye.");
                            keepAsking = false;
                            break;
                        }
                        default:
                        {
                            ConsoleWrite.Line($"The provided answer: {answer}, is not valid. Valid answers are Y or N. Trying again...");
                            break;
                        }
                    }
                }
                else
                {
                    ConsoleWrite.Line("Account details retrieved. Building the JSON...");

                    var json = BuildResultJson(accountDetail);

                    ConsoleWrite.BlankLine();
                    ConsoleWrite.LineWithNoDateTime(json);
                    ConsoleWrite.BlankLine();

                    keepAsking = false;
                }
            }

            ConsoleWrite.Line("Thanks for the challenge =)");
            ConsoleWrite.Line("Press any key to close the app.");
            Console.ReadKey();
        }

        private static AccountDetail LoadData()
        {
            AccountDetail accountDetail = null;

            try
            {
                //Calls the API Client using the AccountClient to retrieve the AccountDetails
                accountDetail = FlinksAPI.AccountClient().GetAccountsDetail();
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    ConsoleWrite.Line(ex.Message);
                }
            }

            return accountDetail;
        }
        private static List<OperationAccount> GetOperationAccountsNumbers(List<Account> accounts)
        {
            var operationAccounts = new List<OperationAccount>();

            var operationAccountNumbers = accounts.Where(x => x.Category == AccountCategoryConstant.Operations).Select(x => x.AccountNumber).ToList();

            foreach (var operationAccountNumber in operationAccountNumbers)
            {
                operationAccounts.Add(new OperationAccount()
                {
                    AccountNumber = operationAccountNumber
                });
            }

            return operationAccounts;
        }
        private static Holder GetHolderInforation(List<Account> accounts)
        {
            var accountDetail = accounts.FirstOrDefault();

            return new Holder()
            {
                Name = accountDetail?.Holder.Name,
                Email = accountDetail?.Holder.Email
            };
        }
        private static List<UsdAccount> GetUsdAccountsBalance(List<Account> accounts)
        {
            var usdAccountsBalance = new List<UsdAccount>();

            var usdAccounts = accounts.Where(x => x.Currency == CurrencyConstant.USD)
                .Select(x => x.Balance.Current)
                .ToList();

            foreach (var usdAccount in usdAccounts)
            {
                usdAccountsBalance.Add(new UsdAccount()
                {
                    Balance = usdAccount.ToString()
                });
            }

            return usdAccountsBalance;
        }
        private static string GetLargestCreditTransactionOfTheLast90Days(List<Account> accounts)
        {
            var lowestDate = DateTimeOffset.Now.AddDays(-90);
            var allTransactions = accounts.Select(x => x.Transactions).ToList();
            var listOfAllTransactions = new List<Transaction>();

            foreach (var transactionList in allTransactions)
            {
                listOfAllTransactions.AddRange(transactionList.Where(x => x.Date.Date >= lowestDate.Date && x.Credit != null));
            }

            listOfAllTransactions = listOfAllTransactions.OrderByDescending(x => x.Credit).ToList();

            return listOfAllTransactions.FirstOrDefault()?.Id.ToString();
        }
        private static string BuildResultJson(AccountDetail accountDetail)
        {
            var result = new Result()
            {
                RequestId = accountDetail.RequestId,
                LoginId = accountDetail.Login?.Id,
                OperationAccounts = GetOperationAccountsNumbers(accountDetail.Accounts),
                Holder = GetHolderInforation(accountDetail.Accounts),
                UsdAccounts = GetUsdAccountsBalance(accountDetail.Accounts),
                BiggestCreditTrxId = GetLargestCreditTransactionOfTheLast90Days(accountDetail.Accounts)
            };


            return JsonConvert.SerializeObject(result, new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore,
                Formatting = Formatting.Indented
            });
        }
    }
}
