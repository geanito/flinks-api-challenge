﻿using System;

namespace Flinks.ApiChallenge.NewClient
{
    public class ClientErrorLogger : IClientErrorLogger
    {
        public void LogError(Exception exception, string errorMessage)
        {
            Console.WriteLine(errorMessage);
        }
    }
}
