﻿using System;
using System.Collections.Generic;
using System.Linq;
using Flinks.ApiChallenge.Domain;
using Flinks.ApiChallenge.Domain.Constant;
using Flinks.ApiChallenge.Domain.Response;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Deserializers;

namespace Flinks.ApiChallenge.NewClient
{
    public sealed class ClientBase : RestClient
    {
        private AuthorizationBody _currentAuthorizationBody;
        private readonly string _user;
        private readonly string _password;
        private readonly string _institution;
        private readonly IClientErrorLogger _errorLogger;

        public ClientBase(IDeserializer deserializer, IClientErrorLogger errorLogger, string baseUrl)
        {
            AddHandler(ClientConstant.ApplicationJson, deserializer);
            _errorLogger = errorLogger;
            BaseUrl = new Uri(baseUrl);
        }

        private void EnsureAuthorization(AuthorizationBody authorizationBody = null)
        {
            if (_currentAuthorizationBody?.RequestId != null) return;

            var authorizationRestClient = new RestClient(BaseUrl);

            var request = new RestRequest(EndpointConstant.Authorize)
            {
                Method = Method.POST,
                RequestFormat = DataFormat.Json
            };

            request.Parameters.Clear();

            request.AddHeader(ClientConstant.ContentType, ClientConstant.ApplicationJson);

            if (authorizationBody != null)
            {
                var json = JsonConvert.SerializeObject(authorizationBody);

                request.AddParameter($"{ClientConstant.ApplicationJson};{ClientConstant.CharsetUTF8}", json, ParameterType.RequestBody);
            }
            else
            {
                request.AddBody(GetAuthorizationBody());
            }

            var httpResponse = authorizationRestClient.Execute(request);

            var authorizationResponse = JsonConvert.DeserializeObject<AuthorizationResponse>(httpResponse.Content);

            switch (authorizationResponse.HttpStatusCode)
            {
                case 203:
                {
                    var answers = FindAnswerToChallenge(authorizationResponse.SecurityChallenges);

                    if (answers.Any())
                    {
                        var newAuthorizationBody = GetAuthorizationBody(authorizationResponse.RequestId, answers);

                        EnsureAuthorization(newAuthorizationBody);
                    }

                    break;
                }
            }
        }
       
        public override IRestResponse Execute(IRestRequest request)
        {
            var response = base.Execute(request);
            CheckForTimeouts(request, response);
            return response;
        }

        public override IRestResponse<T> Execute<T>(IRestRequest request)
        {
            var response = base.Execute<T>(request);
            CheckForTimeouts(request, response);
            return response;
        }

        public T Get<T>(IRestRequest request) where T : new()
        {
            var response = Execute<T>(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return response.Data;
            }
            else
            {
                LogError(BaseUrl, request, response);
                return default(T);
            }
        }


        private void CheckForTimeouts(IRestRequest request, IRestResponse response)
        {
            if (response.StatusCode == 0)
            {
                LogError(BaseUrl, request, response);
            }
        }
        private void LogError(Uri baseUrl, IRestRequest request, IRestResponse response)
        {
            //Get the values of the parameters passed to the API
            var parameters = string.Join(", ", request.Parameters.Select(x => x.Name.ToString() + "=" + (x.Value ?? "NULL")).ToArray());

            //Set up the information message with the URL, the status code, and the parameters.
            var info = $"Request to {baseUrl.AbsoluteUri}{request.Resource} failed with status code {response.StatusCode}, parameters: {parameters}, and content: {response.Content}";

            //Acquire the actual exception
            Exception ex;

            if (response.ErrorException != null)
            {
                ex = response.ErrorException;
            }
            else
            {
                ex = new Exception(info);
                info = string.Empty;
            }

            //Log the exception and info message
            _errorLogger.LogError(ex, info);
        }


        private AuthorizationBody GetAuthorizationBody(string requestId = null, Dictionary<string, List<string>> answerList = null)
        {
            return new AuthorizationBody()
            {
                UserName = _user,
                Password = _password,
                Institution = _institution,
                RequestId = !string.IsNullOrEmpty(requestId) ? requestId : null,
                SecurityResponses = answerList != null && answerList.Any() ? answerList : null
            };
        }
        private static Dictionary<string, List<string>> FindAnswerToChallenge(List<SecurityChallenge> securityChallenges)
        {
            var answersDictionary = new Dictionary<string, List<string>>();

            foreach (var securityChallenge in securityChallenges)
            {
                switch (securityChallenge.Prompt)
                {
                    case SecurityChallengeConstant.WhatCityWereYouBornIn:
                        {
                            answersDictionary.Add(SecurityChallengeConstant.WhatCityWereYouBornIn,
                                new List<string>()
                                {
                                    SecurityChallengeConstant.WhatCityWereYouBornInAnswer
                                });

                            break;
                        }
                    case SecurityChallengeConstant.WhatIsTheBestCountryOnEarth:
                        {
                            answersDictionary.Add(SecurityChallengeConstant.WhatIsTheBestCountryOnEarth,
                                new List<string>()
                                {
                                SecurityChallengeConstant.WhatIsTheBestCountryOnEarthAnswer
                                });

                            break;
                        }
                    case SecurityChallengeConstant.WhatIsTheShapePeopleLikeTheMost:
                        {
                            answersDictionary.Add(SecurityChallengeConstant.WhatIsTheShapePeopleLikeTheMost,
                                new List<string>()
                                {
                                SecurityChallengeConstant.WhatIsTheShapePeopleLikeTheMostAnswer
                                });

                            break;
                        }
                }
            }

            return answersDictionary;
        }
    }
}