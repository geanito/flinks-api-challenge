﻿using System;

namespace Flinks.ApiChallenge.NewClient
{
    public interface IClientErrorLogger
    {
        void LogError(Exception exception, string errorMessage);
    }
}
