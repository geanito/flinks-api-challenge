﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Flinks.ApiChallenge.Domain;
using Flinks.ApiChallenge.Domain.Constant;
using Newtonsoft.Json;
using RestSharp;

namespace Flinks.ApiChallenge.Client
{
    public class ClientBaseOld
    {
        private AuthorizationBody _currentAuthorizationBody;
        private readonly RestClient _restClient;
        private readonly string _user;
        private readonly string _password;
        private readonly string _institution;
        private Action<Exception> NetworkErrorHandler { get; }
        public JsonSerializerSettings JsonSettings => new JsonSerializerSettings()
        {
            TypeNameHandling = TypeNameHandling.All,
            NullValueHandling = NullValueHandling.Ignore
        };
        protected ClientBaseOld(RestClient restClient, string user, string password, string institution, Action<Exception> networkErrorHandler)
        {
            _restClient = restClient;
            _user = user;
            _password = password;
            _institution = institution;
            NetworkErrorHandler = networkErrorHandler;
        }
        public async Task PostAsync(string url, object value)
        {
            try
            {
                EnsureAuthorization();
                var content = new StringContent(JsonConvert.SerializeObject(value), Encoding.UTF8, RequestConstant.ApplicationJson);
                var response = await _restClient.PostAsync(url, content).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
            }
            catch (TaskCanceledException e)
            {
                NetworkErrorHandler(e);
            }
        }
        public async Task<T> PostAsync<T>(string url, object value)
        {
            var jsonResult = string.Empty;

            try
            {
                EnsureAuthorization();
                var content = new StringContent(JsonConvert.SerializeObject(value), Encoding.UTF8, RequestConstant.ApplicationJson);

                var response = await _restClient.PostAsync(url, content).ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                jsonResult = await response.Content.ReadAsStringAsync();
            }
            catch (TaskCanceledException e)
            {
                NetworkErrorHandler(e);
            }

            return !string.IsNullOrEmpty(jsonResult)
                ? JsonConvert.DeserializeObject<T>(jsonResult, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All
                })
                : default(T);
        }
        private void EnsureAuthorization()
        {
            if (_currentAuthorizationBody?.RequestId != null) return;

            var authClient = new HttpClient()
            {
                BaseAddress = _restClient.BaseAddress,
            };

            var authorizationBody = new AuthorizationBody()
            {
                UserName = _user,
                Password = _password,
                Institution = _institution
            };

            var content = new StringContent(JsonConvert.SerializeObject(authorizationBody), Encoding.UTF8, RequestConstant.ApplicationJson);

            var response = authClient.PostAsync(EndpointConstant.Authorize, content).Result;
            response.EnsureSuccessStatusCode();
        }



        //public AuthorizationToken GetAuthToken()
        //{
        //    var authClient = new HttpClient()
        //    {
        //        BaseAddress = _restClient.BaseAddress
        //    };

        //    var content = new FormUrlEncodedContent(
        //        new[]
        //        {
        //            new KeyValuePair<string, string>(UrlParameterConstant.GrantType, UrlParameterConstant.Password),
        //            new KeyValuePair<string, string>(UrlParameterConstant.UserName, _user),
        //            new KeyValuePair<string, string>(UrlParameterConstant.Password, _password)
        //        });

        //    var response = authClient.PostAsync(EndpointConstant.Token, content).Result;
        //    response.EnsureSuccessStatusCode();

        //    var jsonResult = response.Content.ReadAsStringAsync().Result;

        //    return JsonConvert.DeserializeObject<AuthorizationToken>(jsonResult);
        //}

        //private static KeyValuePair<string, string>[] GetUrlParameters(string userName, string password, string institution)
        //{
        //    return new[]
        //    {
        //        new KeyValuePair<string, string>(UrlParameterConstant.UserName, userName),
        //        new KeyValuePair<string, string>(UrlParameterConstant.Password, password),
        //        new KeyValuePair<string, string>(UrlParameterConstant.Institution, institution)
        //    };
        //}
    }
}
