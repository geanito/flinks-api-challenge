﻿using System.Configuration;
using Flinks.ApiChallenge.Domain.Constant;
using RestSharp;

namespace Flinks.ApiChallenge.Client
{
    ///FlinksAPI class works as an aggregator of clients giving the user a single place to use all the API functions
    public static class FlinksAPI
    {
        //In a production environment UserName and Password would be provided by the user
        private static readonly string UserName = ConfigurationManager.AppSettings[FlinksSettingsConstant.UserName];
        private static readonly string Password = ConfigurationManager.AppSettings[FlinksSettingsConstant.Password];
        private static readonly string Institution = ConfigurationManager.AppSettings[FlinksSettingsConstant.Institution];
        private static readonly string CustomerId = ConfigurationManager.AppSettings[FlinksSettingsConstant.CustomerId];

        private static RestClient _currentClient;
        private static AccountClient _testClient;

        private static RestClient GetCurrentClient()
        {
            const string baseUrlConstant = EndpointConstant.BaseUrl;
            var baseUrl = baseUrlConstant.Replace(FlinksSettingsConstant.CustomerIdParameter, CustomerId);

            return _currentClient ?? new RestClient(baseUrl);
        }

        public static AccountClient AccountClient()
        {
            return _testClient ?? new AccountClient(GetCurrentClient(), UserName, Password, Institution);
        }
    }
}
