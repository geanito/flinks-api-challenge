﻿using System.Collections.Generic;
using Flinks.ApiChallenge.Domain.Account;
using Flinks.ApiChallenge.Domain.Constant;
using RestSharp;

namespace Flinks.ApiChallenge.Client
{
    //Account client is an example of how we could use a Client. A Client account can take care of the Account handling
    //If there's another core function, example, Money Transfers, we could add a TransferClient and have their nested methods within that class
    public class AccountClient : ClientBase
    {
        public AccountClient(RestClient restClient, string userName, string password, string institution) : base(restClient, userName, password, institution)
        {
        }

        public AccountDetail GetAccountsDetail()
        {
            //Adds the DaysOfTransactions to the request
            var additionalParameters = new Dictionary<string, string>()
            {
                {ClientParameterConstant.DaysOfTransactions, ClientParameterValueConstant.Days90}
            };

            var request = new RestRequest(EndpointConstant.GetAccountsDetail);

            return Post<AccountDetail>(request, additionalParameters);
        }

    }
}
