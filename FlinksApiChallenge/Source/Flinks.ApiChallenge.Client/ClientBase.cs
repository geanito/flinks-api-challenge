﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Flinks.ApiChallenge.Domain;
using Flinks.ApiChallenge.Domain.Authorization;
using Flinks.ApiChallenge.Domain.Constant;
using Flinks.ApiChallenge.Domain.Shared;
using Newtonsoft.Json;
using RestSharp;

namespace Flinks.ApiChallenge.Client
{
    public class ClientBase
    {
        /*Client base is the main class used to make the requests to the API*/

        private RequestBody ClientRequestBody { get; set; }
        private AuthorizationRequestBody ClientAuthorizationRequestBody { get; set; }
        private RestClient _restClient;
        private readonly string _requestBodyType = $"{ClientParameterConstant.ApplicationJson};{ClientParameterConstant.CharsetUTF8}";
        private readonly JsonSerializerSettings _jsonSerializationSettings = new JsonSerializerSettings()
        {
            //Default Serializer used to put the JSON sent over the requests in the right format.
            NullValueHandling = NullValueHandling.Ignore
        };

        protected ClientBase(RestClient restClient, string userName, string password, string institution)
        {
            ClientRequestBody = new RequestBody();
            ClientAuthorizationRequestBody = new AuthorizationRequestBody()
            {
                UserName = userName,
                Password = password,
                Institution = institution
            };

            _restClient = restClient;
        }

        //Enforces being authenticated on all the requests
        private void EnsureAuthorization()
        {
            if (ClientRequestBody.IsAuthenticated) return;

            _restClient = new RestClient(_restClient.BaseUrl);

            var request = new RestRequest(EndpointConstant.Authorize, Method.POST);
            request.Parameters.Clear();
            request.AddHeader(ClientParameterConstant.ContentType, ClientParameterConstant.ApplicationJson);
            request.AddParameter(_requestBodyType, JsonConvert.SerializeObject(ClientAuthorizationRequestBody), ParameterType.RequestBody);

            var httpResponse = _restClient.Execute(request);

            var authorizationResponse = JsonConvert.DeserializeObject<AuthorizationResponse>(httpResponse.Content);

            switch (authorizationResponse.HttpStatusCode)
            {
                case 200:
                    {
                        if (!string.IsNullOrEmpty(authorizationResponse.RequestId))
                        {
                            ClientRequestBody.RequestId = authorizationResponse.RequestId;
                            ClientRequestBody.LoginId = authorizationResponse.Login?.Id;
                        }

                        break;
                    }
                case 203:
                    {
                        if (authorizationResponse.SecurityChallenges != null && authorizationResponse.SecurityChallenges.Any())
                        {
                            if (!string.IsNullOrEmpty(authorizationResponse.RequestId))
                            {
                                ClientRequestBody.RequestId = authorizationResponse.RequestId;
                            }

                            var answers = FindAnswerToChallenge(authorizationResponse.SecurityChallenges);

                            if (answers.Any())
                            {
                                UpdateAuthorizationBody(answers);
                                EnsureAuthorization();
                            }
                        }

                        break;
                    }
                default:
                    {
                        HandleFlinksValidationDetails(authorizationResponse.ValidationDetails);
                        break;
                    }
            }
        }

        //Generic Method used to post anything to the API synchronously
        public T Post<T>(RestRequest restRequest, Dictionary<string, string> additionalParameters = null) where T : new()
        {
            PreRequestTasks(restRequest, additionalParameters);

            if (!ClientRequestBody.IsAuthenticated) return default(T);

            var response = _restClient.Execute(restRequest);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                var exceptionResponse = JsonConvert.DeserializeObject<FlinksRoot>(response.Content);

                HandleFlinksValidationDetails(exceptionResponse.ValidationDetails);
            }

            return !string.IsNullOrEmpty(response.Content) && response.StatusCode == HttpStatusCode.OK
                ? JsonConvert.DeserializeObject<T>(response.Content)
                : default(T);
        }
        //Executes every time before a request
        private void PreRequestTasks(RestRequest restRequest, Dictionary<string, string> additionalParameters = null)
        {
            EnsureAuthorization();

            restRequest.Method = Method.POST;

            if (additionalParameters != null && additionalParameters.Any())
            {
                AddParametersToClientRequestBody(additionalParameters);
            }

            restRequest.AddParameter(_requestBodyType, JsonConvert.SerializeObject(ClientAuthorizationRequestBody, _jsonSerializationSettings), ParameterType.RequestBody);
        }
        //Updates the AuthorizationBody if a 203 HTTP status is received
        private void UpdateAuthorizationBody(Dictionary<string, List<string>> answerList = null)
        {
            ClientAuthorizationRequestBody.RequestId = ClientRequestBody.RequestId;

            if (answerList != null && answerList.Any())
            {
                ClientAuthorizationRequestBody.SecurityResponses = answerList;
            }
        }
        //Class created to find the answer to the to the SecurityChallenges
        private static Dictionary<string, List<string>> FindAnswerToChallenge(List<SecurityChallenge> securityChallenges)
        {
            var answersDictionary = new Dictionary<string, List<string>>();

            foreach (var securityChallenge in securityChallenges)
            {
                switch (securityChallenge.Prompt)
                {
                    case SecurityChallengeConstant.WhatCityWereYouBornIn:
                        {
                            answersDictionary.Add(SecurityChallengeConstant.WhatCityWereYouBornIn,
                                new List<string>()
                                {
                                    SecurityChallengeConstant.WhatCityWereYouBornInAnswer
                                });

                            break;
                        }
                    case SecurityChallengeConstant.WhatIsTheBestCountryOnEarth:
                        {
                            answersDictionary.Add(SecurityChallengeConstant.WhatIsTheBestCountryOnEarth,
                                new List<string>()
                                {
                                SecurityChallengeConstant.WhatIsTheBestCountryOnEarthAnswer
                                });

                            break;
                        }
                    case SecurityChallengeConstant.WhatIsTheShapePeopleLikeTheMost:
                        {
                            answersDictionary.Add(SecurityChallengeConstant.WhatIsTheShapePeopleLikeTheMost,
                                new List<string>()
                                {
                                SecurityChallengeConstant.WhatIsTheShapePeopleLikeTheMostAnswer
                                });

                            break;
                        }
                }
            }

            return answersDictionary;
        }
        //Uses reflection to add extra parameters to the RequestBody element
        private void AddParametersToClientRequestBody(Dictionary<string, string> additionalParameters)
        {
            foreach (var additionalParameter in additionalParameters)
            {
                if (!ClientRequestBody.GetType().GetProperties().Any(x => x.Name == additionalParameter.Key)) continue;

                var property = ClientRequestBody.GetType().GetProperty(additionalParameter.Key);
                property?.SetValue(ClientRequestBody, Convert.ChangeType(additionalParameter.Value, property.PropertyType), null);
            }
        }
        //Used to read the Validation Issues returned by the API and display the information for the user.
        private void HandleFlinksValidationDetails(Dictionary<string, List<string>> validationDetails)
        {
            ConsoleWrite.Line(FlinksErrorConstant.ValidationProblems);
            ConsoleWrite.BlankLine();

            foreach (var validationDetail in validationDetails)
            {
                foreach (var validationDetailsValue in validationDetail.Value)
                {
                    ConsoleWrite.Line($"{validationDetail.Key}: {validationDetailsValue}");
                }
            }
        }
    }
}
